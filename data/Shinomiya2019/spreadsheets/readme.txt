T4_input_M10.xlsx
T4_output_M10.xlsx
T5_input_Lo1.xlsx
T5_output_Lo1.xlsx

- Each file contains synapse count data of twenty representative T4 or T5 cells.
- 'input' in the file name indicates neurons providing inputs to T4/T5, and 'output' indicates those receiving outputs from T4/T5.
- Each subset of three rows represents connections of a single T4 or T5.
- The second column (highlighted) indicates BodyIDs and neuron names of the target T4/T5 cells.
- Bodies in the third column and beyond are input/output neurons of the target T4 or T5 cells indicated in the second column. They are sorted by the number of synapses they have with the target cell.
- BodyID, neuron name, and synapse number are shown for each body.


Credit: Kazunori Shinomiya/FlyEM Project Team, January 2019
