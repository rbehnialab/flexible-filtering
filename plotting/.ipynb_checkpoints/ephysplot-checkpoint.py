import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv
import sys
import pprint
import math
import pandas as pd
from scipy.optimize import curve_fit

# for finer colorbar control
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar
from itertools import cycle

from analysis.convolve import sigmoid, softplus
from analysis.returnmeans import simple_gaussian, return_mean_temporal, return_mean_fff
from analysis.returnmeans import return_mean_spatial, return_mean_nonlin,return_mean_dg_peaks, fit_nonlin




"""
Functions for Plotting from DataJoint
-----
The following functions were created to plot figures in the ephys analysis
pipeline.

created: January 7, 2020
last updated:

imported from dj_plot_functions.ipynb in
/mnt/engram/notebooks/jportes/StimulusConstruction+Analysis/DataPipelineWork/


* plot_fff()

* plot_xt()

* plot_spatial_analysis()

* plot_temporal()

* plot_wn()

* plot_dg()

* plot_dg_peaks()

TO DO:

* plot_nonlin()

* plot_spatial()

* plot_traces()

* plot_hist() ???

"""

def format_fig_poster(fig):
    """
    This code takes a figure handle and formats it automagically!

    Importantly, it makes large font size for poster presenations, and removes top and right spines
    """

    fntsz1=30
    fntsz2=24

    a = fig.gca()
    a.set_xlabel(a.get_xlabel(),fontsize=fntsz1)
    a.set_ylabel(a.get_ylabel(),fontsize=fntsz1)
    a.set_title(a.get_title(),fontsize=fntsz1)

    a.tick_params(axis='both', which='major', labelsize=fntsz1)
    a.spines['top'].set_visible(False)
    a.spines['right'].set_visible(False)

    a.legend(loc='center left', bbox_to_anchor=(0.75, 0.5),prop={'size': fntsz2}) # loc=1

    fig.tight_layout()

    return fig

def format_fig_paper(fig,width=3,height=2,fontsize=8,legendfontsize=6,linewidth=1,
                     legend=None,tight_layout=False,
                     remove_title=True,**kwargs):
    
    """ format figure for publication
    
    width: in inches
    height: in inches
    fontsize: real font size
    """
    
    
    
    fig.set_size_inches(width,height) # in inches
    
    ax = fig.get_axes() # formatted as list    
    
    for a in ax:

        """ fontsize """
        for item in ([a.title, a.xaxis.label, a.yaxis.label] + a.get_xticklabels() + a.get_yticklabels()): # + a.get_legend().get_texts()
            item.set_fontsize(fontsize)
            
        """ linewidth """
        lines = a.get_lines()
        plt.setp(lines, linewidth=linewidth)
        
        # remove old legend
        if a.get_legend() is not None:
            #a.get_legend().remove()
            
            """ fontsize """
            for item in a.get_legend().get_texts():
                item.set_fontsize(legendfontsize)
        
        if legend is not None:
            
            a.legend(legend,frameon=False, fontsize=fontsize)
        
    if tight_layout:
        fig.tight_layout()
    
    return fig,ax






"""The following are for PRESENTATION purposes and try to convey the mean and std"""

def plot_spatial_mean(wn_list,**kwargs):

    """
    This plots the mean spatial receptive field for a list of white noise filters

    Args
    ----
    wn_list: Ordered List of dict objects, queried and fetched from datajoint
        e.g. wn_list = (RecordingInfo * RecordingData * WNData).fetch(as_dict=True)

    Returns
    -------
    fig: figure handle

    """

    if 'fig' in kwargs.keys():
        fig = kwargs['fig']
        ax = fig.gca()
    else:
        fig, ax = plt.subplots(1,1,figsize=(10,5))


    if 'color' in kwargs.keys():
        color = kwargs['color']
    else:
        color = 'C0'

    """This function from axolotl.traces.analysis returns a dict with the spatial mean"""
    spatial = return_mean_spatial(wn_list,plot_figure=False)

    # should only be one cell type and one bath solution in wn_list e.g. Tm9
    cell_type = np.unique([wn['cell_type'] for wn in wn_list])[0]
    bath_solution = np.unique([wn['bath_solution'] for wn in wn_list])[0]
    n = len(wn_list)
    #print(cell_type)

    y = spatial['mean']*1000 # plot in mV
    std = spatial['std']*1000 # plot in mV
    plt.plot(spatial['x'],y,color=color,linewidth=3,label=cell_type + ' '+ bath_solution +', n='+str(n))
    plt.fill_between(spatial['x'],y - std, y + std,alpha=0.2,color=color)
    #plt.ylim(top = 0.0008)
    plt.xlim(-60,60)
    ax.set_ylabel('amplitude (mV)')
    ax.set_xlabel('space (\u00b0)')

    return fig


def plot_nonlin_mean(wn_list,nonlin='softplus',bin_size=200,sigma_weight=None,**kwargs):

    """
    Plot static nonlinearities from list of datajoint queried objects

    Args
    ----
    wn_list: Ordered List of dict objects, queried and fetched from datajoint
        e.g. wn_list = (RecordingInfo * RecordingData * WNData).fetch(as_dict=True)
    nonlin: string either 'softplus' or 'sigmoid'
    bin_size: bin size for average
    sigma_weight: either None or 'std' for weighting fit


    Returns
    ----
    fig: figure handle

    """

    if 'plot_fit' in kwargs.keys():
        plot_fit = kwargs['plot_fit']
    else:
        plot_fit = False


    # allows plotting two nonlinearities from different cells/scenarios on same fig
    if 'fig' in kwargs.keys():
        fig = kwargs['fig']
        ax = fig.gca()
    else:
        fig, ax = plt.subplots(1,1,figsize=(8,8))


    plt.ylim(-25,25)
    plt.xlim(-25,25)
    plt.plot([-25,25],[-25,25],'gray')
    ax.axvline(color='gray')
    ax.axhline(color='gray')

    if 'color' in kwargs.keys():
        color = kwargs['color']
    else:
        color = 'C0'


    # Plot bins for individual cells
    binned_dict = return_mean_nonlin(wn_list,bin_size=bin_size)

    y_mean = binned_dict['y_mean']*1000 # mV
    y_std = binned_dict['y_std']*1000
    y_pred_mean = binned_dict['y_pred_mean']*1000
    # plot mean
    plt.plot(y_pred_mean,y_mean,linewidth=3,color=color,label=wn_list[0]['cell_type']+'-'+wn_list[0]['bath_solution'])
    plt.fill_between(y_pred_mean,y_mean - y_std, y_mean + y_std, color=color, alpha=0.2)



    """Plot fit to mean"""
    if plot_fit:
        fit = fit_nonlin(wn_list,nonlin=nonlin,sigma_weight=sigma_weight) # this fits the average

        x = np.linspace(np.min(fit['y_pred_fit_nonan']),np.max(fit['y_pred_fit_nonan']),400)

        #fit['mean_popt'] = popt

        #print(nonlin)
        if fit['nonlin'] == 'sigmoid':
            k=fit['popt'][0]
            L=fit['popt'][1]
            x0 = fit['popt'][2]
            b = fit['popt'][3]

            #plt.plot(x,sigmoid(x,k,L,x0,b),'o',linewidth=4)
            plt.plot(x,sigmoid(x,k,L,x0,b),'-',color=color,linewidth=3,alpha=1)
            plt.title('sigmoid nonlinearity',fontsize=30)


        if fit['nonlin'] == 'softplus':
            a = fit['popt'][0]
            b = fit['popt'][1]
            c = fit['popt'][2]
            d = fit['popt'][3]
            k = fit['popt'][4]

            #plt.plot(x,softplus(x,a,b,c,d,k),'o',linewidth=4)
            plt.plot(x,softplus(x,a,b,c,d,k),'-',color=color,linewidth=3,alpha=1,label=('a={:.1f},b={:.2f},c={:.3f},d={:.3f},k={:.2f}').format(a,b,c,d,k))
            plt.title('softplus nonlinearity',fontsize=30)

    plt.legend(fontsize=12)
    ax.tick_params(axis='both', which='major', labelsize=24)


    #plt.xlim(-np.max(minmax),np.max(minmax))
    plt.xlabel('linear input (mV)',fontsize=30)
    plt.ylabel('nonlinear output (mV)',fontsize=30)
    plt.grid(which='both')
    plt.tight_layout()

    return fig



# updated
def plot_dg_peaks_mean(dg_list,normalize=False,**kwargs):
    """
    Plot min-max difference to drifting grating reponse on log scale with labels.
    Handles either spatially and temporally changing stimuli

    Args
    ----
    dg_list: list of (ordered) dicts queried from datajoint
    normalize: boolean, whether to normalize across cells. Passed on to function dg_peaks_mean()

    Returns
    -------
    fig: figure handle
    """

    # allows plotting two nonlinearities from different cells/scenarios on same fig
    if 'fig' in kwargs.keys():
        fig = kwargs['fig']
        ax = fig.gca()
    else:
        fig, ax = plt.subplots(1,1,figsize=(8,6))
    #fig, ax = plt.subplots(1,1,figsize=(6,4))

    if 'color' in kwargs.keys():
        color = kwargs['color']
    else:
        color='C0'


    prop_cycle = plt.rcParams['axes.prop_cycle']
    av_colors = cycle(prop_cycle.by_key()['color'])

    # plot average
    if len(dg_list) > 1:

        dg_mean = return_mean_dg_peaks(dg_list,normalize=normalize)

        y = dg_mean['dg_mean']
        y_std = dg_mean['dg_std']
        plt.plot(1/dg_mean['freq_array'],y,'o-',linewidth=3,color=color,label='mean')
        plt.fill_between(1/dg_mean['freq_array'],y - y_std, y + y_std, color=color, alpha=0.2)

    ax.set_xscale('log')
    ax.set_ylim(0,0.04)
    plt.grid(b=True, which='both')
    plt.xlabel(dg_mean['name']+' Period (T)') # 'Square Sine Wave '+
    plt.ylabel('Amplitude ('+r'$\Delta V$'+')') #
    #plt.title('Drifting Grating Response')
    plt.legend()

    return fig
    





"""The following are for ANALYSIS purposes and try to convey as much information
as possible in the plots"""


# Plot each individually

def plot_fff(fff_list,category,duration,baseline,av_within_cell=True,legend=True):

    """
    Function to plot full field flash data

    There are 3 subplots:
    1. data
    2. data with baseline subtracted
    3. average of all data in fff_list

    Some cells were presented with full field flashes multiple times, and therefore
    a single 'recording_id' might have multiple subrecording_numbers. This gives us
    the choice of averaging within a cell followed by averaging across cells, or
    averaging across all individual recordings.

    Args
    ----
    fff_list: list, list of ordered dicts queried with datajoint
    category: string, 'on_repeats' or 'off_repeats'
    duration: float, in seconds (e.g. 1)
    baseline: string, 'start' or 'end'
    av_within_cell=True: boolean
        Average within cell first, then average across cells in fff_list
    legend=True: boolean


    Returns
    -------
    fig: figure handle

    (I cleaned up this function on January 7, 2020)
    """
    fig,ax = plt.subplots(3,1,figsize=(8,8))

    traces = [] # the mean of this is taken at the end and plotted in ax[2]

    # >>> AVERAGE WITHIN CELL <<<
    # check for non_unique recording_id values and average
    if av_within_cell:
        print('>> Average within cell')

        r_id = [r['recording_id'] for r in fff_list] # all recording ids

        for r in np.unique(r_id):

            # find index of unique/repeated recording ids
            idx = np.where(np.asarray(r_id)==r)[0]

            #print(idx.tolist())
            #print(np.asarray(r_id)[idx])

            av_within = []
            sub_rec_list = [] # keeps track of subrecording id, for book keeping
            for i in idx: # this works for list of 1 as well as larger
                f = fff_list[i]

                dt = f['sampling_rate'] # presumably these are the same, otherwise throw error

                trace = np.mean(f[category],1)
                trace_cut = trace[:int(int(duration)/dt)] # necessary for occasional mismatch lengths
                av_within.append(trace_cut)

                sub_rec_list.append(f['subrecording_number'].zfill(3))

            trace = np.mean(av_within,0) # this treats av_within as a single trace
            trace_std = np.std(av_within,0) # this should be zero if list idx is size 1

            t = np.linspace(0,len(trace)*dt,len(trace))

            ax[0].plot(t,trace,label=f['recording_id']+'-'+'/'.join(sub_rec_list))
            ax[0].fill_between(t,trace - trace_std, trace + trace_std,alpha=0.2)
            ax[0].set_title(category+' (average within cell)')

            if legend:
                ax[0].legend(loc=1)

            if baseline == 'start':
                b = trace[0]
            else:
                b = np.mean(trace[-100:])

            ax[1].plot(t,trace-b,label=f['recording_id']+'-'+'/'.join(sub_rec_list))
            ax[1].fill_between(t,trace-b - trace_std, trace-b + trace_std,alpha=0.2)
            ax[1].set_title('Zero Baseline')
            ax[2].plot(t,trace-b,alpha=0.25,label=f['recording_id'])

            traces.append(trace-b)

    # >>> DON"T AVERAGE WITHIN CELL <<<
    if av_within_cell==False:
        print('>> No average within cell')

        for f in fff_list:

            dt = f['sampling_rate']

            trace = np.mean(f[category],1) # this treats av_within as a single trace
            trace = trace[:int(int(duration)/dt)] # necessary for occasional mismatch lengths

            t = np.linspace(0,len(trace)*dt,len(trace))

            ax[0].plot(t,trace,label=f['recording_id']+'-'+f['subrecording_number'].zfill(3))
            ax[0].set_title(category+' (no average within cell)')
            if leg:
                ax[0].legend(loc=1)

            if baseline == 'start':
                b = trace[0]
            else:
                b = np.mean(trace[-100:])

            ax[1].plot(t,trace-b,label=f['recording_id']+'-'+f['subrecording_number'].zfill(3))
            ax[1].set_title('Zero Baseline')
            ax[2].plot(t,trace-b,alpha=0.25,label=f['recording_id'])

            traces.append(trace-b)

    mn = np.mean(traces,0)
    ax[2].plot(t,mn,'k',linewidth=2,label='mean')
    std = np.std(traces,0)
    ax[2].fill_between(t,mn - std, mn + std, color='k', alpha=0.2)

    ax[2].set_title('Average Response, n={:.0f}'.format(len(traces)))
    ax[2].set_ylim(-0.025,0.025)

    for a in ax:
        a.set_xlim(0,10)
        if legend:
            a.legend(loc=1)
        a.set_xlabel('time (s)')
        a.set_xlabel('time (s)',fontsize=14)
        a.set_ylabel('amplitude (V)',fontsize=14)

        a.tick_params(axis='both', which='major', labelsize=14)
        a.spines['top'].set_visible(False)
        a.spines['right'].set_visible(False)
        a.grid(which='major')

    plt.tight_layout()

    return fig



def plot_xt(wn_list):

    """
    Plot spatiotemporal receptive fields
    """

    for w in wn_list:

        fig, ax = plt.subplots(1,2,figsize=(8,4),squeeze=False)

        im = np.real(w['linear_filter'])
        vmin = np.min(im)
        vmax = np.max(im)
        vlim = np.max(np.abs([vmin, vmax]))

        ax[0,0].imshow(im,aspect='auto', cmap='bwr',vmin = -vlim,
                  vmax=vlim)
        ax[0,0].set_title('Linear Filter, Unweighted')

        image = w['complete_filter']
        im = ax[0,1].imshow(image,aspect='auto', cmap='bwr',vmin = -vlim,
                  vmax=vlim)
        ax[0,1].set_title('Complete Filter, '+r'$R^2=$'+'{:.2f}'.format(w['score']))

        ax[0,0].set_xlabel('X')
        ax[0,0].set_ylabel('T')

        # automatically include colorbar for last axis
        ax_divider = make_axes_locatable(ax[-1,-1])
        cax = ax_divider.append_axes("right", size="7%", pad="2%")
        cbar = fig.colorbar(im, cax=cax, orientation='vertical')
        #cbar.ax.tick_params(labelsize=fntsz1)


        #plt.colorbar()
        fig.suptitle(w['cell_type'] + '-'+w['recording_id']+'-'+w['subrecording_number'].zfill(3),va='top',y=1.1,fontsize=16)
        plt.tight_layout()
        plt.show()
    return fig


def plot_spatial(wn_list,legend=True,**kwargs):

    """
    Plot spatial receptive field (1-d array)

    This function is for analysis purposes, and creates a figure with subplots 2x3
    """

    # legend is optional for when there are too many things to plot...
    if 'p0' in kwargs.keys():
        p0 = kwargs['p0']
    else:
        p0 = [30,4,1e-3] # mu, sig1,scale

    # this is overwritten/overriden below
    # if 'dx' in kwargs.keys():
    #     dx = kwargs['dx']
    #     assert(dx is None,'dx is None')
    # else:
    #     dx = 5

    # used for finding units around max peak, [ind-dt:ind+dt,:]
    if 'dt' in kwargs.keys():
        dt = kwargs['dt']
    else:
        dt = 1

    # wrapper function to fit gaussian
    def fit_curve(x,p0):

        # preset to max location
        p0[0] = np.argmax(sp)*dx
        #print(p0)

        bounds=([-130,0,-20], [130,30,20])

        popt, pcov = curve_fit(simple_gaussian, x, sp,p0=p0,bounds=bounds)
        return popt

    #c = plt.rcParams['axes.prop_cycle'].by_key()['color']
    #c = plt.cm.viridis(np.linspace(0,1,len(wn_list)))

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = cycle(prop_cycle.by_key()['color'])

    # Plot each cell extracted from database
    fig,ax = plt.subplots(2,3,figsize=(16,6))
    for i,w in enumerate(wn_list):

        # this comes from DataJoint RecordingInfo()
        dx = float(w['wn_bar_width'])

        # find index of temporal peak point
        ind = np.argmin(w['temporal_filter'])

        # unusual case, except when downsampling
        if ind <= 2:
            dt = 1



        # Now use unweighted components
        ax[0,0].set_title('Unweighted Spatial Components')

        sp = -1*np.mean(np.real(w['linear_filter'])[ind-dt:ind+dt,:],0)
        std = -1*np.std(np.real(w['linear_filter'])[ind-dt:ind+dt,:],0)

        x = np.arange(len(sp))*dx # either 2.5 or 5 degrees


        ax[0,0].plot(x,sp,label=w['cell_type'] + '-'+w['recording_id']+'-'+w['subrecording_number'].zfill(3))
        ax[0,0].fill_between(x,sp - std, sp + std,alpha=0.2)

        #if legend:
        ax[0,0].legend(loc=4)

        #
        ax[0,1].set_title('Centered (average over {:.0f} temporal points)'.format(dt*2+1) )

        popt=fit_curve(x,p0)

        x1 = np.arange(len(sp))*dx-popt[0]
        ax[0,1].plot(x1,sp,label=w['cell_type'] + '-'+w['recording_id']+'-'+w['subrecording_number'].zfill(3))
        ax[0,1].fill_between(x1,sp - std, sp + std,alpha=0.2)

        # keep colors the same for fits
        c = next(colors)

        # Plot Gaussian Fit
        ax[0,2].set_title('Gaussian Fit')
        ax[0,2].plot(x1,sp,color=c,alpha=0.5,linewidth=3)
        ax[0,2].fill_between(x1,sp - std, sp + std,color=c,alpha=0.2)

        x2= np.linspace(-10,10,100)*dx
        ax[0,2].plot(x2,simple_gaussian(x=x2,mu=0,sig=popt[1],scale=popt[2]),'--',color=c,label=r'$\sigma=$'+str(np.round(popt[1],2)))
        if legend:
            ax[0,2].legend()
        plt.tight_layout()



        #ind = np.argmin(w['temporal_filter'])
        #ind = np.argmin(w['complete_filter'][ind,:],1)
        #print(ind)

        # Now use x_weighted components
        ax[1,0].set_title('Corrected (Weighted) Spatial Components')

        sp = -1*np.mean(w['complete_filter'][ind-dt:ind+dt,:],0)
        std = -1*np.std(np.real(w['complete_filter'])[ind-dt:ind+dt,:],0)

        x = np.arange(len(sp))*dx # since they are all 2.5 or 5 degrees
        ax[1,0].plot(x,sp,label=w['cell_type'] + '-'+w['recording_id']+'-'+w['subrecording_number'].zfill(3))
        ax[1,0].fill_between(x,sp - std, sp + std,alpha=0.2)

        #peak = np.argmax(sp)

        popt=fit_curve(x,p0)

        x1 = np.arange(len(sp))*dx-popt[0]
        ax[1,1].plot(x1,sp,label=w['cell_type'] + '-'+w['recording_id']+'-'+w['subrecording_number'].zfill(3))
        ax[1,1].fill_between(x1,sp - std, sp + std,alpha=0.2)

        ax[1,1].set_title('Weighted Spatial Components, Centered')

        # Plot Gaussian Fit
        ax[1,2].set_title('Gaussian Fit')

        ax[1,2].plot(x1,sp,color=c,alpha=0.5,linewidth=3)
        ax[1,2].fill_between(x1,sp - std, sp + std,color=c,alpha=0.2)

        x2= np.linspace(-10,10,100)*dx
        ax[1,2].plot(x2,simple_gaussian(x=x2,mu=0,sig=popt[1],scale=popt[2]),'--',color=c,label=r'$\sigma=$'+str(np.round(popt[1],2)))
        if legend:
            ax[1,2].legend()
        plt.tight_layout()

    for a in np.ravel(ax):
        a.set_xlabel('space (\u00b0)')
        a.grid(which='major')
        plt.tight_layout()
    #plt.show()

    return fig



# Plot each individually

def plot_temporal(wn_list,baseline,**kwargs):

    """
    Plot 1-d temporal trace in figure with 1 subplot
    """

    if 'legend' in kwargs.keys():
        leg = kwargs['legend']
    else:
        leg = True

    fig,ax = plt.subplots(1,1,figsize=(6,3))

    av = []
    n=0

    tmp_recording_id = str('')

    for m in wn_list:



        trace = m['temporal_filter']
        trace = trace[:499]

        dt = m['filter_time']/len(trace)

        t = np.linspace(0,len(trace)*dt,len(trace))

        ax.plot(t,trace,label=m['cell_type']+'-'+m['recording_id']+'-'+m['subrecording_number'].zfill(3))
        if leg:
            ax.legend(loc=1)

        if baseline == 'start':
            b = trace[0]
        else:
            b = np.mean(trace[-100:])

        #ax[1].plot(t,trace-b,label=m['cell_type']+'-'+m['recording_id']+'-'+m['subrecording_number'].zfill(3))
        #ax[2].plot(t,trace-b,alpha=0.25,label=m['cell_type']+'-'+m['recording_id']+'-'+m['subrecording_number'].zfill(3))

        av.append(trace-b)

        if tmp_recording_id != m['recording_id']:
            n+=1
        tmp_recording_id = m['recording_id']


    mn = np.squeeze(np.mean(av,0))

    ax.plot(t,mn,'k',linewidth=2,label='average')
    std = np.squeeze(np.std(av,0))
    ax.fill_between(t,mn - std, mn + std, color='k', alpha=0.2)

    ax.set_title('n={:.0f}'.format(n))
    #ax[2].set_ylim(-0.01,0.025)


    a =ax
    a.set_ylim(-0.002,0.0006)
    #a.set_xlim(0,2)
    if leg:
        a.legend(loc=1)
    a.set_xlabel('time (s)')
    a.set_xlabel('time (s)',fontsize=14)
    a.set_ylabel('amplitude (V)',fontsize=14)

    a.tick_params(axis='both', which='major', labelsize=14)
    a.spines['top'].set_visible(False)
    a.spines['right'].set_visible(False)

    a.grid(which='major')

    plt.tight_layout()
    #plt.show()

    return fig



# Plotting Drifting Gratings

def plot_dg(dg_list,**kwargs):
    """
    Function for plotting response to drifting gratings. Plots mean and std on temporal axis

    Args
    ----
    dg_list: list of (ordered) dicts queried from datajoint

    The parameters that are necessary:
        psths: np.array (stim type,time) e.g. 11x20000
        psths_std: np.array same dimensions as above
        repeat_dur: np.array, single dimension of stim type e.g. 11
        spatialfrequency_array: np.array of spatial frequencies
        temporalfrequency_array: np.array of temporal frequencies
        sampling_rate: float, necessary for time array

    Returns
    -------fig: figure handle
    """

    if 'title' in kwargs.keys():
        title = kwargs['title']
    else:
        title=True


    for dg in dg_list:

        fig, ax = plt.subplots(1,1,figsize=(6,4))

        spatialfrequency_array = dg['spatialfrequency_array']
        temporalfrequency_array = dg['temporalfrequency_array']
        sampling_rate = dg['sampling_rate']

        # make sure this is correct
        psths = dg['psths']
        psths_std = dg['psths_std']
        repeat_dur = dg['repeat_duration']

        # sneaky trick to make unzipping easier, might not be necessary
        psths = psths.tolist()
        psths_std = psths_std.tolist()

        # Check if changing spatial or temporal frequency
        if np.median(spatialfrequency_array) != spatialfrequency_array[0]:
            freq_array = spatialfrequency_array
            label = 'deg'

        if np.mean(temporalfrequency_array) != temporalfrequency_array[0]:
            freq_array = temporalfrequency_array
            label = 'sec'

        stim_colors = cycle(plt.cm.viridis(np.linspace(0,1,len(spatialfrequency_array))))

        for (p,std,freq,rep) in zip(psths,psths_std,freq_array,repeat_dur):

            c = next(stim_colors)

            y = p[:rep]-np.min(p[:rep])
            t = np.linspace(0,len(y)*sampling_rate,len(y))

            ax.plot(t,y, label='{:.2f} '.format(1/freq)+label,color=c)
            ax.fill_between(t,y - std[:rep], y + std[:rep],alpha=0.2,color=c)

        if title:
            plt.title(dg['cell_type']+' '+dg['recording_id']+'-'+
                            dg['subrecording_number'].zfill(3)+'  '+
                            dg['bath_solution']+' '+dg['stimulus_name'])
        plt.legend(loc=1)
        plt.xlabel('time (s)')
        plt.ylim(0,0.04)
        plt.grid(b=True, which='both')
        plt.ylabel('Amplitude (V)')
        #plt.show() # plot after returning handle

    return fig

def plot_dg_peaks(dg_list):
    """
    Plot min-max difference to drifting grating reponse on log scale with labels.
    Handles either spatially and temporally changing stimuli

    Args
    ----
    dg_list: list of (ordered) dicts queried from datajoint

    The parameters that are necessary:
        psths: np.array (stim type,time) e.g. 11x20000
        psths_std: np.array same dimensions as above
        repeat_dur: np.array, single dimension of stim type e.g. 11
        spatialfrequency_array: np.array of spatial frequencies
        temporalfrequency_array: np.array of temporal frequencies
        sampling_rate: float, necessary for time array

    Returns
    -------fig: figure handle
    """
    fig, ax = plt.subplots(1,1,figsize=(6,4))

    prop_cycle = plt.rcParams['axes.prop_cycle']
    av_colors = cycle(prop_cycle.by_key()['color'])


    av_peaks = []
    for dg in dg_list:

        spatialfrequency_array = dg['spatialfrequency_array']
        temporalfrequency_array = dg['temporalfrequency_array']
        sampling_rate = dg['sampling_rate']

        # make sure this is correct
        psths = dg['psths']
        psths_std = dg['psths_std']
        repeat_dur = dg['repeat_duration']

        # psths = extracted_dict['psths']
        # psths_std = extracted_dict['psths_std']
        # repeat_dur = extracted_dict['repeat_dur']

        # Check if changing spatial or temporal frequency
        if np.median(spatialfrequency_array) != spatialfrequency_array[0]:
            freq_array = spatialfrequency_array
            name = 'Spatial'

        if np.mean(temporalfrequency_array) != temporalfrequency_array[0]:
            freq_array = temporalfrequency_array
            name = 'Temporal'

        peaks = []



        stim_colors = cycle(plt.cm.viridis(np.linspace(0,1,len(spatialfrequency_array))))

        for (p,std,freq,rep) in zip(psths,psths_std,freq_array,repeat_dur):

            c = next(stim_colors)

            dif = np.max(p[:rep]) - np.min(p[:rep])
            dif_std = np.sqrt(std[np.argmax(p[:rep])]**2 + std[np.argmin(p[:rep])]**2) # is this correct?
            plt.errorbar(1/freq,dif,yerr=dif_std,marker='d',color=c)

            peaks.append(dif)



        lab = dg['cell_type']+' '+dg['recording_id']+'-'+dg['subrecording_number'].zfill(3)+'  '+dg['bath_solution']+' '+dg['stimulus_name']
        plt.plot(1/freq_array,peaks,'--',alpha = 1,label=lab,color=next(av_colors))

        #plt.show() # plot after returning handle


        av_peaks.append(peaks)

    # plot average
    if len(dg_list) > 1:
        plt.plot(1/freq_array,np.squeeze(np.mean(av_peaks,0)),'k',label='mean')

    # TO DO
    # CALCULATE SHARED VARIANCE AND PLOT SHARED VARIANCE

    ax.set_xscale('log')
    ax.set_ylim(0,0.04)
    plt.grid(b=True, which='both')
    plt.xlabel('Square Sine Wave '+name+' Period (T)')
    plt.ylabel('Amplitude Difference ('+r'$\Delta V$'+')')
    plt.title('Drifting Grating Response')
    plt.legend()

    return fig



    # Plot Nonlinearity HERE

    
    
    
    
    
    
    
# updated    
""" PLOTTING FUNCTIONS """



""" Plot 20,40,80 and 160 flashes with and without OA """

def plot_flash_data_saline_OA(flash_dict,colors= ['darkmagenta','violet'],
                              ylim=(-0.012*1e3,0.02*1e3),
                              xlim=(-0.1,3),sc = 1e3,**kwargs):

    """ 
    
    This function plots saline and OA flashes for 20, 40, 80 and 160 ms stored as mean and std in dict 
    
    Args
    ----
    flash_dict: Dictionary, extracted via the function 'extract_flash_data'. 
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'
    
    Returns
    -------
    fig: Figure handle
    
    """
    
    plt.rcParams.update({'font.size': 14})

    if 'fig' in kwargs.keys():
        fig = kwargs['fig']
        ax = fig.get_axes()
    else:
        fig,ax = plt.subplots(1,4,figsize=(12,3))
        
     # convert to mV
        
        
    count = 0
    for k,tr in flash_dict.items():
        
        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002
        
        
        if count < 4:
            c = colors[0]
            count+=1
        else:
            c = colors[1]
        
        if 'saline' in k:
            alpha=0.2
        else:
            alpha=0.4
        
        if '20' in k:
            ax[0].plot(time,tr['mean']*sc,color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[0].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)
            ax[0].set_title('20 ms')
        if '40' in k:
            ax[1].plot(time,tr['mean']*sc,color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[1].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)
            ax[1].set_title('40 ms')
        if '80' in k:
            ax[2].plot(time,tr['mean']*sc,color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[2].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)
            ax[2].set_title('80 ms')
        if '160' in k:
            ax[3].plot(time,tr['mean']*sc,color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[3].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0) # removes line
            ax[3].set_title('160 ms')

    for i,a in enumerate(ax):
        a.set_ylim(ylim)
        a.set_xlim(xlim)
        a.legend(frameon=False)

        if i >0:
            a.axis('off')

        else:
            a.spines['top'].set_visible(False)
            a.spines['right'].set_visible(False)
            a.set_ylabel('V')
            a.set_xlabel('time (s)')

    #print('>>TM4')


#     figure_folder = '/mnt/engram/notebooks/jportes/DS-Project-Paper/Figures/Figures-Gilliam-2020-9-4/'
#     """ Save figure """
#     var = input("Save figures? (y/n): ")
#     if var == 'y':
#         fig.savefig(figure_folder+'flash-OA-Tm4.pdf')
#         print('saved figure: ',figure_folder+'flash-OA-Tm4.pdf')
        
    return fig





def plot_flash_and_pred(wn_pred_dict,flash_dict,bath_solution='saline',colors = ['darkmagenta','violet'],
                        ylim=(-0.012*1e3,0.025*1e3),xlim=(-0.1,3),sc=1e3,**kwargs):
    
    """
    
    This function plots saline (or OA) flashes with white noise filter predictions for 20, 40, 80 and 160 ms stored as mean and std in dict 
    
    Args
    ----
    wn_pred_dict: Dictionary, extracted from flash_wn_pred function.
                Dictionary contains keys for 4 conditions: 20,40,80,160. Value for each key is a list of 1D temporal predictions
    
    flash_dict: Dictionary, extracted via the function 'extract_flash_data'. 
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'
    bath_solution: str, either 'saline' or 'OA'             
    colors: list of two color strings (e.g. ['darkmagenta','violet'])
    
    Returns
    -------
    fig: Figure handle
    
    """
    
    if 'fig' in kwargs.keys():
        fig = kwargs['fig']
        ax = fig.get_axes()
    else:
        fig,ax = plt.subplots(1,4,figsize=(12,3))
        
    

    mean20 = np.mean(wn_pred_dict['20'],axis=0)*sc
    mean40 = np.mean(wn_pred_dict['40'],axis=0)*sc
    mean80 = np.mean(wn_pred_dict['80'],axis=0)*sc
    mean160 = np.mean(wn_pred_dict['160'],axis=0)*sc

    std20 = np.std(wn_pred_dict['20'],axis=0)*sc
    std40 = np.std(wn_pred_dict['40'],axis=0)*sc
    std80 = np.std(wn_pred_dict['80'],axis=0)*sc
    std160 = np.std(wn_pred_dict['160'],axis=0)*sc

    t_pred = np.arange(0,3,0.01)

    c1 = 'black'

    """ Plot flash Data """
    count=0
    for k,tr in flash_dict.items():
        
        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002

        
        if count < 4:
            c = colors[0]
            count+=1
        else:
            c = colors[1]
            
        if 'saline' in k:
            alpha = 0.2
        else:
            alpha = 0.4
            
        if bath_solution in k:
            #c = next(colors)
            if '20' in k:
                ax[0].plot(time,tr['mean']*sc,label=k,color=c,linewidth=3)
                ax[0].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)

                ax[0].plot(t_pred,mean20,'--',label=k,color=c1,linewidth=3)
                ax[0].fill_between(t_pred,mean20+std20,mean20-std20,color=c1,alpha=alpha,linewidth=0.0)
                ax[0].set_title('20 ms')
            if '40' in k:
                ax[1].plot(time,tr['mean']*sc,label=k,color=c,linewidth=3)
                ax[1].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)

                ax[1].plot(t_pred,mean40,'--',label=k,color=c1,linewidth=3)
                ax[1].fill_between(t_pred,mean40+std40,mean40-std40,color=c1,alpha=alpha,linewidth=0.0)
                ax[1].set_title('40 ms')
            if '80' in k:
                ax[2].plot(time,tr['mean']*sc,label=k,color=c,linewidth=3)
                ax[2].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)

                ax[2].plot(t_pred,mean80,'--',label=k,color=c1,linewidth=3)
                ax[2].fill_between(t_pred,mean80+std80,mean80-std80,color=c1,alpha=alpha,linewidth=0.0)
                ax[2].set_title('80 ms')
            if '160' in k:
                ax[3].plot(time,tr['mean']*sc,label=k,color=c,linewidth=3)
                ax[3].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)

                ax[3].plot(t_pred,mean160,'--',label=k,color=c1,linewidth=3)
                ax[3].fill_between(t_pred,mean160+std160,mean160-std160,color=c1,alpha=alpha,linewidth=0.0)
                ax[3].set_title('160 ms')



    for i,a in enumerate(ax):
        a.set_ylim(ylim)
        a.set_xlim(xlim)
        if i >0:
            a.axis('off')

        else:
            a.spines['top'].set_visible(False)
            a.spines['right'].set_visible(False)
            a.legend(['flash data','WN pred'],frameon=False)
            a.set_ylabel('mV')
            a.set_xlabel('time (s)')

    
    

    # figure_folder = '/mnt/engram/notebooks/jportes/DS-Project-Paper/Figures/Figures-Gilliam-2020-9-4/'
    # """ Save figure """
    # var = input("Save figures? (y/n): ")
    # if var == 'y':
    #     fig.savefig(figure_folder+'flash-wnpred-Tm9.pdf')
    #     print('saved figure: ',figure_folder+'flash-wnpred-Tm9.pdf')

    return fig




def plot_flash_contrast_data(flash_dict_low,
                             flash_dict_high,
                             bath_solution='saline',
                             colors= ['dimgray','violet'],
                             xlim=(-0.1,3),ylim=(-0.012*1e3,0.02*1e3),
                             sc=1e3,ylabel='mV',figsize=(6,3),
                             **kwargs):

    """ This function plots flashes for low contrast and high contrast stored in separate dicts. Assumes only 20 ms and 160 ms data
    
    
    Args
    ----
    flash_dict_low: Dictionary, extracted via the function 'extract_flash_data'. Should contain low contrast data
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int_lowcontrast0to01 saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'
                
    flash_dict_high: Dictionary, extracted via the function 'extract_flash_data'. Should contain high contrast data
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'
    
    bath_solution: str, 'saline' or OA
    colors: list of 2 color strings
    
    
    Returns
    -------
    fig: Figure handle
    
    
    """
    
    plt.rcParams.update({'font.size': 14})

    fig,ax = plt.subplots(1,2,figsize=figsize)
    count = 0
    
    
    if 'saline' in bath_solution:
        alpha = 0.2
    else:
        alpha = 0.4

    for k,tr in flash_dict_high.items():
        
        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002
        
        c = colors[1]
        

        
        if bath_solution in k:
            if '20' in k:
                ax[0].plot(time,tr['mean']*sc,color=c,linewidth=3,label='0-1 (n='+str(tr['n'])+')')
                ax[0].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)
                ax[0].set_title('20 ms')

            if '160' in k:
                ax[1].plot(time,tr['mean']*sc,color=c,linewidth=3,label='0-1 (n='+str(tr['n'])+')')
                ax[1].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)
                ax[1].set_title('160 ms')
                
                
    for k,tr in flash_dict_low.items():
        
        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002
        
            
        c = colors[0]
        
        if bath_solution in k:
            if '20' in k:
                ax[0].plot(time,tr['mean']*sc,color=c,linewidth=3,label='0-0.1 (n='+str(tr['n'])+')')
                ax[0].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)
                ax[0].set_title('20 ms')

            if '160' in k:
                ax[1].plot(time,tr['mean']*sc,color=c,linewidth=3,label='0-0.1 (n='+str(tr['n'])+')')
                ax[1].fill_between(time,tr['mean']*sc+tr['std']*sc,tr['mean']*sc-tr['std']*sc,color=c,alpha=alpha,linewidth=0.0)
                ax[1].set_title('160 ms')

    for i,a in enumerate(ax):
        a.set_ylim(ylim)
        a.set_xlim(xlim)
        a.legend(frameon=False)

        if i >0:
            a.axis('off')

        else:
            a.spines['top'].set_visible(False)
            a.spines['right'].set_visible(False)
            a.set_ylabel(ylabel)
            a.set_xlabel('time (s)')
            
            
    return fig