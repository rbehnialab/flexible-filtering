import warnings
warnings.filterwarnings("ignore")

import os
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys
import pprint
import math
import time #counting for filter extraction
from itertools import cycle
# from tqdm import tqdm
from copy import copy, deepcopy
import pickle
import numpy.linalg as LA

from scipy.optimize import curve_fit
from scipy.signal import lfilter, decimate
from scipy.stats import binned_statistic, pearsonr
from scipy.io import loadmat

from matplotlib.backends.backend_pdf import PdfPages
import datetime

""" Linear Regression """
from sklearn.linear_model import Lasso, LinearRegression
from sklearn.metrics import r2_score
from sklearn.model_selection import ParameterGrid

# for pdf generation
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1 import make_axes_locatable



"""

* score_flash_and_pred

* plot_pred_flash_score


"""


#####################################
#  MAIN LINEAR REGRESSION FUNCTION  #
#####################################

def fit_gruntman_stationary_flashes(gruntman_data, 
                                    pred_dict_list,
                                    flash_dict_list,
                                    dur='0.16', # or 0.04 or '0.16'
                                    width = '9', # or '2.25','4.5', '9'
                                    prediction_set='white_noise', # 'white_noise' or 'flash_data'
                                    list_keys=['Tm1','Tm9'], # e.g. 'Tm1','Tm2','Tm4','Tm9'
                                    dt_sample = 0.01,
                                    g_t = np.linspace(1,45000,45000)/20-18000/20,
                                    window = 100,
                                    ds = 50,
                                    fit_intercept=False,
                                    bath_solution='saline',
                                    positive_weights=True,
                                    verbose=False,plot= False):
    
    """ 
    Given a list of possible inputs to T5, apply linear regression to gruntman stationary flash data.
    
    This can apply to white noise filter predictions of flashes (pred_dict_list), which must be created before running function
    This can also apply to experimental flash data
    
    Inputs
    ------
    g_data: gruntman data stored in dict
    dur:
    width:
    prediction_set:
    list_keys:
    pred_dict_list: e.g. [Tm1_pred_dict,Tm9_pred_dict]
    flash_dict_list: e.g. [Tm1_flash_dict,Tm9_flash_dict]
    dt_sample:
    g_t:
    window: int, how many timebins to select (e.g. at dt = 0.01, 100 timebins is equivalent to 1 second )
    ds: int, eg. downsample flash data from 5 Khz to 100 Hz with ds=50. This is used for Jessie's Tm1-Tm9 data
    fit_intercept: boolean, passed to linear regression model
    bath_solution: 'saline' or 'OA'
    positive_weights: bool, passed to linear regression model
    verbose:
    plot: bool
    
    
    Returns
    -------
    fit_dict: dictionary with linear regression results/attributes
    
        'prediction_set'
        'dur'
        'width'
        'list_keys'
        'x_data'
        'x_data_std'
        'coefs'
        'intercepts'
        'r2s'
    
    """
    
    g_data = gruntman_data # shorthand for more concise code
    
    # original T5 traces are sampled at 20 kHz, are length 45000
    # stimulus appears at 0.9 seconds i.e. 18000 ms
    """ gruntman time parameters """
    dt_sample = 0.01 
    g_t = (np.linspace(1,45000,45000)-18000)/20 # set by experimental data
    dt_g = np.round(np.unique(np.diff(g_t))[0],4)*1e-3


 
    
    """ gruntman time parameters """
    
    dt_g = np.round(np.unique(np.diff(g_t))[0],4)*1e-3
    if verbose: print('dt_g = {}'.format(dt_g))
    if verbose: print('sampling freq = {} Hz'.format(1/dt_g))
    if verbose: print('window size: ',window)
        
        
        
    """ Lasso Model 
    alpha = 1, imposes L1 constraint
    alpha = 0, equivalent to linear regression

    positive=True sets coefficients to be positive. This is the reason I am using Lasso here
    """
    model = Lasso(alpha=0.0001,
                  fit_intercept=fit_intercept,
                  precompute=True,
                  max_iter=1000,
                  positive=positive_weights, 
                  random_state=9999, 
                  selection='random') # alpha=0.0001
    
    if verbose: print('fit_intercept=',fit_intercept)
    
    
    """ 
    Loop through dur keys ['0.04', '0.16']
    then width keys ['2.25', '4.5', '9']
    finally, location keys e.g. final level in ['9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
    (althouh some have ['9', '10', '11'v, '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'])
    """

    

    """ 
    White Noise Convolution Prediction 

    These can just be extracted from Tm1_pred_dict,Tm2_pred_dict etc.
    """
    
    # this is already downsampled to 100 Hz, and 3 seconds long
    # converts from '0.04' key to '40'
    dur_str = str(np.int(np.float(dur)*1e3))
    
    if prediction_set == 'white_noise':
        lr = {}
        lr_std={}
        
        for i,key in enumerate(list_keys):
            
            # mean from each item of pred_dict_list
            lr[key] = np.nanmean(pred_dict_list[i][dur_str],axis=0)[:window]
            
            # std
            lr_std[key] = np.nanvar(pred_dict_list[i][dur_str],axis=0)[:window]
            
            

    if prediction_set == 'flash_data':
        lr = {}
        lr_std = {}

        
        
        for i,key in enumerate(list_keys):
            # mean from each item of pred_dict_list
            # downsample from Jessie's flash data 5 kHz to 100 Hz with ::50, and then select from window
            
            """ IS THIS 50 OR 100????"""
            
            lr[key] = flash_dict_list[i]['FFF_uniform_'+dur_str+'msimpulse_10int '+bath_solution]['mean'][::ds][:window] 
            
            # std
            lr_std[key] = flash_dict_list[i]['FFF_uniform_'+dur_str+'msimpulse_10int '+bath_solution]['std'][::ds][:window]**2 # want variance
            

            
    """ concatenate and convert to np.array """
    
    """ mean """
    concat_list = []
    for i,key in enumerate(list_keys):
        concat_list.append(lr[key]/np.max(lr[key]))
    
    x_data = np.asarray(concat_list).T
    
    """ std """
    concat_list = []
    for i,key in enumerate(list_keys):
        """ if mean is scaled by amount 'a' (in this case np.max(lr[key])), std is scaled by a and variance is scaled by a^2"""
        concat_list.append(lr_std[key]/np.max(lr[key]**2))
    
    x_data_std = np.asarray(concat_list).T

    
    #loc_array = np.asarray(list(dic2.keys())).astype(np.int) - 16
    # hardcoded
    #loc_array = np.arange(-6,7).astype(np.int)
    loc_array = np.arange(-6,8).astype(np.int)

    coefs = np.zeros((len(loc_array), x_data.shape[1]))
    intercepts = np.zeros(len(x_data))
    r2s = np.zeros((len(g_data[dur][width].items())))
    pearsonrs = np.zeros((len(g_data[dur][width].items())))
    



    """ Fit Each Trace """
    for count,(loc, trace) in enumerate(g_data[dur][width].items()):

        ax_ind = loc_array[count] + 6 # hardcoded from Gruntman et al. 2019

        trace_cut = trace['mean'][int(0.9/dt_g):] # chop/cut trace, note np.where(g_t>=0)[0][0]*dt_g = 0.89995
        trace_cut_std = trace['std'][int(0.9/dt_g):]
        trace_ds = trace_cut[::200][:window] # downsample to 100 Hz from 20 kHz
        trace_ds_std = trace_cut_std[::200][:window]

        y_data = trace_ds

        y_data[np.isnan(y_data)]=0


        """ Fit Model Here """

        model.fit(x_data, y_data)

        coefs[count,:] = model.coef_
        intercepts[count] = model.intercept_

        if verbose: print("model slope:    ", model.coef_)
        if verbose: print("model intercept:", model.intercept_)

        s = model.score(x_data,y_data)
        r2s[count] = s
        
        pr = pearsonr(model.predict(x_data).reshape(-1,),y_data.reshape(-1,))
        if verbose: print('pearsonr: ',pr)
        pearsonrs[count] = pr[0] # first value
        
#     coef_dict[dur][width] = coefs
#     r2_dict[dur][width] = r2s
               

    """ save as dict """
    fit_dict={}
    fit_dict['prediction_set']=prediction_set
    fit_dict['dur'] = dur
    fit_dict['width'] = width
    fit_dict['list_keys'] = list_keys
    fit_dict['x_data'] = x_data
    fit_dict['x_data_std'] = x_data_std
    fit_dict['coefs'] = coefs
    fit_dict['intercepts'] = intercepts
    fit_dict['r2s'] = r2s
    fit_dict['pearsonrs'] = pearsonrs

    return fit_dict




def plot_gruntman_traces(gruntman_data,
                         fit_dict,
                         dur='0.16', # or 0.04
                         width = '9',
                         window = 100,
                         colors=['C0','orange'],
                         suptitle=None,
                         figsize=(20,4),
                         select_loc = ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'],
                         **kwargs):
    
    """
    Plot stationary flash data from gruntman et al., superimposed with
    model (linear regression) predictions stored in `fit_dict`
    
    gruntman_data: dict, loaded from '/mnt/engram/notebooks/jportes/Data/GruntmanData/trace_data_v6-18a.pickle'
        keys are: 
            * ['0.04', '0.16'] # duration
                * ['2.25', '4.5', '9'] # width
                    * ['9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22']
                    * ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23']
                    * ['11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
    
    fit_dict: dict, output of fit_gruntman_stationary_flashes(), defined above
    
    
    dur: str
    width: 
    window:
    select_loc: list of strings, allow for plotting of only specific locations. This is based on Gruntman MATLAB code =/
            e.g. ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23']
            
            
    Corresponding degrees (from Gruntman MATLAB code):
    (np.arange(-6,8).astype(np.int))*2.25
    array([-13.5 , -11.25,  -9.  ,  -6.75,  -4.5 ,  -2.25,   0.  ,   2.25,
             4.5 ,   6.75,   9.  ,  11.25,  13.5 ,  15.75])
    
    """
    
    g_data = gruntman_data
    
    # original T5 traces are sampled at 20 kHz, are length 45000
    # stimulus appears at 0.9 seconds i.e. index 18000 
    """ gruntman time parameters """
    dt_sample = 0.01 
    g_t = (np.linspace(1,45000,45000)-18000)/20 # set by experimental data
    dt_g = np.round(np.unique(np.diff(g_t))[0],4)*1e-3

    
    """ plot gruntman data stationary traces along with fits from 
    function fit_gruntan_stationary_flashes()
    """
    loc_array = np.arange(-6,8).astype(np.int) # based on Gruntman data
    
    fig,ax = plt.subplots(1,len(select_loc),figsize=figsize)
    
    if suptitle is not None:
        fig.subplots_adjust(top=0.8)
        fig.suptitle(suptitle) #, y=0.98)

    """ PLOT TRACES AND FITS AT EACH LOCATION """
    
    
    
    """ Fit Each Trace """
    ax_ind=0
    for count,(loc,trace) in enumerate(g_data[dur][width].items()):
        
        """ allow for plotting of only select locations"""
        if loc not in select_loc:
            continue
    
        #ax_ind = loc_array[count] + 6 # hardcoded from Gruntman et al. 2019
        
        
        trace_cut = trace['mean'][int(0.9/dt_g):] # chop/cut trace, note np.where(g_t>=0)[0][0]*dt_g = 0.89995
        trace_cut_std = trace['std'][int(0.9/dt_g):]
        trace_ds = trace_cut[::200][:window] # downsample to 1000 Hz from 20 kHz
        trace_ds_std = trace_cut_std[::200][:window]

        y_data = trace_ds

        y_data[np.isnan(y_data)]=0

        
        tt = np.arange(len(trace_ds))*0.01
        ax[ax_ind].plot(tt,trace_ds,'--',color=colors[0],linewidth=2,label='T5 data')#, loc={}'.format(loc_array[count]))
        ax[ax_ind].fill_between(tt,trace_ds-trace_ds_std,trace_ds+trace_ds_std,alpha=0.15,color=colors[0])

        x_pred = fit_dict['x_data'].dot(fit_dict['coefs'][count,:]) +fit_dict['intercepts'][count]
        ax[ax_ind].plot(tt,x_pred,'-',linewidth=3,color=colors[1],label='fit') #  R2={:.2f}'.format(fit_dict['r2s'][count])) # 'fit Tm1={:.1f} Tm4={:.1f},Tm9={:.1f}, R2={:.2f}'.format(*model.coef_,s)

        
        """ plot std carried forward via sum of variance for each component
        note that scaling of variance should be a^2, not a
        then take sqrt of sum of variances
        """
        x_pred_std = np.sqrt(fit_dict['x_data_std'].dot(fit_dict['coefs'][count]**2)) # +fit_dict['intercepts'][count] pred already contains intercept
        ax[ax_ind].fill_between(tt,x_pred-x_pred_std,x_pred+x_pred_std,color='grey',alpha=0.2,linewidth=0) # 'fit Tm1={:.1f} Tm4={:.1f},Tm9={:.1f}, R2={:.2f}'.format(*model.coef_,s)

        ax_ind += 1

    ax[ax_ind-1].legend()
    
    # Formatting
    for i,a in enumerate(ax):
        a.spines['right'].set_visible(False)
        a.spines['top'].set_visible(False)

        #plt.title('loc={}, score={:.3f}'.format(loc,model.score(x_data.reshape(-1,2),y_data)))

        a.set_ylim(-8,25)

        if i > 0:
            a.spines['left'].set_visible(False)
            a.spines['bottom'].set_visible(False)

            a.tick_params(
                axis='both',       # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                left=False,
                right=False,
                labelbottom=False,
                labelleft=False,
            )

    return fig




def plot_gruntman_fit_rfs(fit_dict,names,figsize=(16,4)):
    
    """
    2D plots of receptive fields weighted by linear regression coefficients
    
    fit_dict: dict, output of fit_gruntman_stationary_flashes() defined above
    names: list
    figsize: tuple
    
    Returns
    -------
    fig: figure handle. Increase fig size depending on how many components are in fit_dict
    
    
    Corresponding degrees (from Gruntman MATLAB code):
    (np.arange(-6,8).astype(np.int))*2.25
    array([-13.5 , -11.25,  -9.  ,  -6.75,  -4.5 ,  -2.25,   0.  ,   2.25,
             4.5 ,   6.75,   9.  ,  11.25,  13.5 ,  15.75])
    
    """
    
    
    """ plot receptive fields that result from fits """
    x_data = fit_dict['x_data']
    coefs = fit_dict['coefs']
    
    fig, ax = plt.subplots(1,x_data.shape[1]+1,figsize=figsize) 

    """ Plot Spatiotemporal RF """
    map_list = []
    
    for i in range(x_data.shape[1]):
        map_list.append(np.repeat(x_data[:,i].reshape(-1,1),coefs.shape[0],axis=1)* coefs[:,i])


    map_list.append(np.sum(np.asarray(map_list),axis=0).squeeze())
    
    
    vlim = 15 # hardcoded
    ylim = len(x_data)*10
    xlim = map_list[0].shape[1]*2.25

    
    for i in range(x_data.shape[1]):
        ax[i].imshow(map_list[i],cmap='bwr',aspect='auto',extent=[0,xlim,0,ylim],vmin=-vlim,vmax=vlim)

    
    # get handle for last one for colorbar
    im = ax[-1].imshow(map_list[-1],cmap='bwr',aspect='auto',extent=[0,xlim,0,ylim],vmin=-vlim,vmax=vlim)
    ax[-1].contour(map_list[-1],colors='k',extent=[0,xlim,0,ylim],origin='upper')

    
    for i, name in enumerate(names):
        ax[i].set_title(name)

    combo = names[0]
    for i in range(len(names)-1):
        combo = combo + '+' + names[i+1]
    
    ax[-1].set_title(combo)

    
    # automatically include colorbar for last axis
    ax_divider = make_axes_locatable(ax[-1])
    cax = ax_divider.append_axes("right", size="7%", pad="2%")
    cbar = fig.colorbar(im, cax=cax, orientation='vertical')
    #cbar.ax.tick_params(labelsize=fntsz1)


    ax[0].set_ylabel('time (ms)')
    for a in ax:
        labels = a.get_yticks()
        a.set_yticklabels(np.flip(labels*1e-3))
        a.set_xlabel('space (degrees)')
        #a.set_xticks(labels*2.25)
        #print(labels)
        a.set_xlim(0,31.5)
        
    fig.tight_layout()
                
    return fig


def plot_coefs(fit_dict,
               colors=['midnightblue','firebrick','darkmagenta','saddlebrown'],
               alpha=1,
               figsize=(5,3),
               peak=True,**kwargs):
    
    """
    Plot linear regression coefficients as 1D plots
    
    fit_dict: dict, output of fit_gruntman_stationary_flashes() defined above
    colors: list of strings, should be in order of components in fit_dict
    peak: bool, if True, find peak of each component and plot vertical line
    
    (np.arange(-6,8).astype(np.int))*2.25
    array([-13.5 , -11.25,  -9.  ,  -6.75,  -4.5 ,  -2.25,   0.  ,   2.25,
         4.5 ,   6.75,   9.  ,  11.25,  13.5 ,  15.75])
    
    """
    
    if 'fig' in kwargs.keys():
        fig= kwargs['fig']
        ax = fig.gca()
    else:
        fig, ax = plt.subplots(1,1,figsize=figsize)
    
    loc_array = np.arange(-6,8)*2.25
    
    pks=[]
    for i,coef in enumerate(fit_dict['coefs'].T):
        ax.plot(loc_array,coef,color=colors[i],linewidth=4,alpha=alpha)
        
        
        # find peak
        pk = np.argmax(coef)
        pks.append(pk)
        if peak: ax.axvline(loc_array[pk],linestyle='--',color=colors[i],linewidth=3,alpha=0.5)
        
    dif = np.mean(np.abs(np.diff(pks)))
    print(np.abs(np.diff(pks)))
        
    ax.set_xlabel('spatial location')
    if peak: ax.text(0.9, 0.9, r'$\Delta x =$'+str(dif*2.25)+u"\u00b0", horizontalalignment='center',verticalalignment='center', transform=ax.transAxes)
    
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
        
    return fig





#################################################








# This could be in a different location?
def score_flash_and_pred(wn_pred_dict,
                         flash_dict,
                         bath_solution='saline',
                         downsample=100,
                         verbose=False,
                         **kwargs):
    
    """
    
    This function calculates r2_score for saline (or OA) flashes with white noise filter predictions for 20, 40, 80 and 160 ms stored as mean and std in dict 
    
    Args
    ----
    wn_pred_dict: Dictionary, extracted from flash_wn_pred function.
                Dictionary contains keys for 4 conditions: 20,40,80,160. Value for each key is a list of 1D temporal predictions
    
    flash_dict: Dictionary, extracted via the function 'extract_flash_data'. 
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'
    bath_solution: str, either 'saline' or 'OA'           
    
    downsample: int in Hz
    
    Returns
    -------
    score_dict: dict of scores
    
    """
    
    
#     fig,ax = plt.subplots(1,4,figsize=(12,3))

    mean20 = np.mean(wn_pred_dict['20'],axis=0)
    mean40 = np.mean(wn_pred_dict['40'],axis=0)
    mean80 = np.mean(wn_pred_dict['80'],axis=0)
    mean160 = np.mean(wn_pred_dict['160'],axis=0)

    std20 = np.std(wn_pred_dict['20'],axis=0)
    std40 = np.std(wn_pred_dict['40'],axis=0)
    std80 = np.std(wn_pred_dict['80'],axis=0)
    std160 = np.std(wn_pred_dict['160'],axis=0)

    t_pred = np.arange(0,3,0.01)


    score_dict={}
    score_dict['20'] = {}
    score_dict['40'] = {}
    score_dict['80'] = {}
    score_dict['160'] = {}

    """ Plot flash Data """

    
    window = len(mean20) # all predictions should be same length, 300 = 3 seconds
    
    
    
    for k,tr in flash_dict.items():
        
        ds = np.int(1/(downsample*tr['sampling_rate'])) # e.g. 1/(100 Hz 0.0002 sampling rate), equivalent to 5000/100
        if verbose: print('ds = int(1/(downsample*tr["sampling_rate"])) = ',ds)
        
        if bath_solution in k:
            if '20' in k:
                
                score_dict['20']['r2']=r2_score(tr['mean'][::ds][:window],mean20)
                score_dict['20']['corrcoef']=np.corrcoef(mean20,tr['mean'][::ds][:window])

            if '40' in k:
                
                score_dict['40']['r2']=r2_score(tr['mean'][::ds][:window],mean40)
                score_dict['40']['corrcoef']=np.corrcoef(mean40,tr['mean'][::ds][:window])
                
            if '80' in k:
                
                score_dict['80']['r2']=r2_score(tr['mean'][::ds][:window],mean80)
                score_dict['80']['corrcoef']=np.corrcoef(mean80,tr['mean'][::ds][:window])

            if '160' in k:
                score_dict['160']['r2']=r2_score(tr['mean'][::ds][:window],mean160)
                score_dict['160']['corrcoef']=np.corrcoef(mean160,tr['mean'][::ds][:window])


                
                plt.plot(tr['mean'][::ds][:window])
                plt.plot(mean160)
    return score_dict



def plot_pred_flash_score(score_dict_list,
                          width=0.9,
                          ylim=(-0.5,1),
                          colors=['midnightblue','firebrick','darkmagenta','saddlebrown']):
    
    """
    
    This function creates a bar plot of R2 values for wn pred vs. flash data
    
    score_dict_list: list of score dicts, e.g. [Tm1_score_dict,Tm2_score_dict,Tm4_score_dict,Tm9_score_dict]
        need to use function score_flash_and_pred(Tm1_pred_dict,Tm1_flash_dict,bath_solution='saline',verbose=False)
        
    width: float, width of bar plot
    
    
    NOTE: I could also plot pearson's correlation coeficient
    """

    fig,ax = plt.subplots(1,1,figsize=(6,4))
    n = len(score_dict_list)
    
    for i,score_dict in enumerate(score_dict_list):
        for j,(k,d) in enumerate(score_dict.items()):
            #print(k)

            ax.bar(j-width/n+i*width/n,d['r2'],width/n,color=colors[i])
            #plt.plot(k,d['corrcoef'][0,1],'x')

    ax.set_ylim(ylim)
    ax.set_xticks([0,1,2,3])
    ax.set_yticks(np.arange(-0.4,1.2,0.2))
    ax.set_xticklabels(['20','40','80','160'])
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.tick_params(
                axis='x',          # changes apply to the y-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off

                labelleft=False,
                labelsize=16) # labels along the bottom edge are off

    ax.tick_params(axis='y',labelsize=14)
    #ax.legend(frameon=False)
    ax.set_ylabel(r'$R^2$')
    ax.set_title(r'$R^2$'+': white noise pred vs. flash')
    
    return fig









def plot_flash_superimposed(flash_dict_list,
                            duration='160',
                            solution='saline',
                            dy=0.03,
                            colors=['darkmagenta', 'violet'],
                            xlim=(-0.1,0.5),
                            figsize=(1.5,4),
                            alpha=0.3,
                            **kwargs):
    
    
    """ Plot Impulse Responses on top of eachother 

    This function is purely for schematic purposes. It takes a flash_dict_list
    and plots responses without a y axis in a small figure
    """
    
    fig,ax = plt.subplots(1,1,figsize=figsize)
    colors = cycle(colors)

    """ Plot flash Data """
    y_offset = 0
    for flash_dict in flash_dict_list:
        for k,tr in flash_dict.items():
            #print(k)
            if (solution in k) and (duration in k):

                time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002

                c = next(colors)


                ax.plot(time,y_offset+tr['mean'],label=k,color=c,linewidth=3)
                ax.fill_between(time,y_offset+tr['mean']+tr['std'],y_offset+tr['mean']-tr['std'],color=c,alpha=alpha,linewidth=0)
                ax.axhline(y_offset,color='gray',alpha=0.3)
                ax.set_title(duration+' ms')
                y_offset-=dy

    for a in [ax]:
        #a.set_ylim(-0.03,0.03)
        a.set_xlim(xlim)
        a.spines['top'].set_visible(False)
        a.spines['right'].set_visible(False)
        a.spines['left'].set_visible(False)
        
        a.axvline(int(duration)*1e-3,color='gray',alpha=0.3)
        a.axvline(0,color='gray',alpha=0.3)
        
        
        a.tick_params(
            axis='y',          # changes apply to the y-axis
            which='both',      # both major and minor ticks are affected
            left=False,      # ticks along the bottom edge are off
            right=False,         # ticks along the top edge are off
            labelleft=False) # labels along the bottom edge are off
        a.legend(frameon=False)
        a.get_legend().remove()
        
    return fig


def plot_wn_pred_superimposed(wn_pred_dict_list,
                              duration='160',
                              solution='saline',
                              dy=0.03,
                              colors=['darkmagenta', 'violet'],
                              xlim=(-0.1,0.5),
                              sampling_rate=1/100,
                              figsize=(1.5,4),
                              alpha=0.3,
                              **kwargs):
    
    """ Plot Impulse Responses on top of eachother 

    This function is purely for schematic purposes. It takes a wn_pred_dict_list
    and plots responses without a y axis in a small figure.
    
    It is almost the same as the function above, but uses wn_pred_dicts instead
    """
    
    fig,ax = plt.subplots(1,1,figsize=figsize)
    colors = cycle(colors)

    """ Plot flash Data """
    y_offset = 0
    for wn_pred_dict in wn_pred_dict_list:
        
        tr={}
        tr['mean']=mn= np.mean(wn_pred_dict[duration],axis=0)

        tr['std']=std = np.std(wn_pred_dict[duration],axis=0)
        
        #for k,tr in wn_dict.items():
            #print(k)
        # if (solution in k) and (duration in k):

        time = np.arange(0,len(tr['mean']))*sampling_rate # should be 0.0002

        c = next(colors)


        ax.plot(time,y_offset+tr['mean'],color=c,linewidth=3)
        ax.fill_between(time,y_offset+tr['mean']+tr['std'],y_offset+tr['mean']-tr['std'],
                        color=c,alpha=alpha,linewidth=0)
        ax.set_title(duration+' ms')
        y_offset-=dy

    for a in [ax]:
        #a.set_ylim(-0.03,0.03)
        a.set_xlim(xlim)
        a.spines['top'].set_visible(False)
        a.spines['right'].set_visible(False)
        a.spines['left'].set_visible(False)
        
        a.axvline(int(duration)*1e-3,color='gray',alpha=0.3)
        a.axvline(0,color='gray',alpha=0.3)
        
        a.tick_params(
            axis='y',          # changes apply to the y-axis
            which='both',      # both major and minor ticks are affected
            left=False,      # ticks along the bottom edge are off
            right=False,         # ticks along the top edge are off
            labelleft=False) # labels along the bottom edge are off
        #a.legend(frameon=False)
        #a.get_legend().remove()
        
    return fig
    
    
    
    
    
    
    
    
""" Moving Bar Functions """


def plot_moving_bar_data(moving_data,
                         width,dur,
                         mb_pred_dict=None,
                         window=300,
                         ds=2,
                         figsize=(6,3),
                         colors=['green','green'],
                         pred_colors=['darkgreen','darkgreen'],
                         linestyles=['-','--'],
                         ylims=(-10,25),
                         xlims=(0,3),
                         alpha=0.2,
                         **kwargs):
    
    """ 
    Plot moving bar data from Gruntman 2019 et al.
    PD and ND
    
    
    
    Returns
    -------
    
    fig: figure handle
    """
    
    if mb_pred_dict is not None:
        pd = np.max(mb_pred_dict['pd'])
        nd = np.max(mb_pred_dict['nd'])
        pred_dsi = np.round((pd-nd)/(pd+nd),3)
        print('DSI:',pred_dsi)

    fig,ax = plt.subplots(1,2,figsize=figsize)

    y_pd = moving_data[width][dur]['pd_mean']
    y_pd_std = moving_data[width][dur]['pd_std']
    y_nd = moving_data[width][dur]['nd_mean']
    y_nd_std = moving_data[width][dur]['nd_std']

    # downsample by 2 # << MAKE SURE THIS IS CORRECT
    y_pd = y_pd[::ds][:window]
    y_pd_std = y_pd_std[::ds][:window]
    y_nd = y_nd[::ds][:window]
    y_nd_std = y_nd_std[::ds][:window]

    tt_ds = np.arange(0,len(y_pd)*0.01-0.01,0.01)
    if len(tt_ds) != len(y_pd):
        tt_ds = np.arange(0,len(y_pd)*0.01,0.01)        

    ax[0].plot(tt_ds,y_pd,linewidth=2,color=colors[0],label='T5 moving PD data',alpha=0.6,linestyle=linestyles[0])
    ax[0].fill_between(tt_ds,y_pd-y_pd_std,y_pd+y_pd_std,alpha=alpha,color=colors[0],linewidth=0)
    ax[1].plot(tt_ds,y_nd,linewidth=2,color=colors[1],label='T5 moving ND data',alpha=0.6,linestyle=linestyles[0])
    ax[1].fill_between(tt_ds,y_nd-y_nd_std,y_nd+y_nd_std,alpha=alpha,color=colors[0],linewidth=0)

    data_dsi = (np.max(y_pd)-np.max(y_nd))/(np.max(y_pd)+np.max(y_nd))
    ax[1].legend(['Data DSI {:.2f}'.format(data_dsi)])

    """ if pred dict is included """
    if mb_pred_dict is not None:
        
        pd_pred = mb_pred_dict['pd'][:len(tt_ds)]
        nd_pred = mb_pred_dict['nd'][:len(tt_ds)]
        
        #scale = np.max(y_pd)/np.max(pd_pred)
        
        # can either be normalized by PD, or fit with LR
        scale = mb_pred_dict['scale']
        
        ax[0].plot(tt_ds,scale*pd_pred,linewidth=3,label='pred moving PD data',linestyle=linestyles[1],color=pred_colors[0])
        ax[1].plot(tt_ds,scale*nd_pred,linewidth=3,label='pred moving PD data',linestyle=linestyles[1],color=pred_colors[0])

        ax[0].text(0.9,0.9,'gain={:.2f}'.format(scale),horizontalalignment='center',verticalalignment='center', transform=ax[0].transAxes)
        ax[1].text(0.9,0.9,'gain={:.2f}'.format(scale),horizontalalignment='center',verticalalignment='center', transform=ax[1].transAxes)

    
        ax[1].legend(['Data DSI {:.2f}'.format(data_dsi),'pred DSI {:.2f}'.format(pred_dsi)])
    
    plt.title('dur={}, width={}'.format(dur,width))
    for a in ax:
        a.spines['right'].set_visible(False)
        a.spines['top'].set_visible(False)
        a.spines['left'].set_visible(False)
        #a.spines['bottom'].set_visible(False)
        #a.legend(bbox_to_anchor=(1.2, 1))
        a.tick_params(
            axis='x',          # changes apply to the x-axis
            which='minor',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False) # labels along the bottom edge are off
        #plt.legend()
        a.set_ylim(ylims)
        a.set_xlim(xlims)
    ax[1].tick_params(
            axis='y',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            left=False,         # ticks along the left edge are off
            labelleft=False) # labels along the bottom edge are off
    
    ax[0].set_title('PD')
    ax[1].set_title('ND')
    
    plt.tight_layout()
    
    
    return fig








def predict_moving_bar(fit_dict,
                        moving_data=None,
                        dur = '0.16',
                        width= '4.5',
                        flash_dict_list = None,
                        pred_dict_list = None,
                        bath_solution = 'saline',
                        prediction_set = 'flash_data',
                        list_keys=['Tm1','Tm9'],
                        ds = 50,
                        sample_window= 200,
                        r2_window=200,
                        traces_to_fit = 'data',
                        figsize=(8,3),
                        ylims=(-30,40),
                        colors=['green','green'],
                        component_colors = ['midnightblue','saddlebrown'],
                        plot=True,verbose=True):
    
    
    """
    Predict Moving Bar Data using `fit_dict`
    
    fit_dict: dict, output of function
    
    flash_dict_list: e.g. [Tm1_flash_dict,Tm9_flash_dict]
    pred_dict_list: e.g. [Tm1_pred_dict,Tm9_pred_dict]
    
    Returns
    -------
    moving_bar_dict: dict, formatted similar to `fit_dict`
    
    """


    # this is already downsampled to 100 Hz, and 3 seconds long
    # converts from '0.04' key to '40'
    dur_str = str(np.int(np.float(dur)*1e3))

    if prediction_set == 'white_noise':
        lr = {}
        lr_std={}

        for i,key in enumerate(list_keys):

            # mean from each item of pred_dict_list
            lr[key] = np.mean(pred_dict_list[i][dur_str],axis=0)[:sample_window]

            # std
            lr_std[key] = np.var(pred_dict_list[i][dur_str],axis=0)[:sample_window]



    if prediction_set == 'flash_data':
        lr = {}
        lr_std = {}



        for i,key in enumerate(list_keys):
            # mean from each item of pred_dict_list
            # downsample from Jessie's flash data 5 kHz to 100 Hz with ::50, and then select from window


            lr[key] = flash_dict_list[i]['FFF_uniform_'+dur_str+'msimpulse_10int '+bath_solution]['mean'][::ds][:sample_window] 

            # std
            lr_std[key] = flash_dict_list[i]['FFF_uniform_'+dur_str+'msimpulse_10int '+bath_solution]['std'][::ds][:sample_window]**2 # want variance



    """ concatenate and convert to np.array """

    """ mean """
    concat_list = []
    for i,key in enumerate(list_keys):
        concat_list.append(lr[key]/np.max(lr[key]))

    if traces_to_fit == 'fit_dict':
        x_data = fit_dict['x_data']
    elif traces_to_fit == 'data':
        x_data = np.asarray(concat_list).T

    if plot: fig,ax = plt.subplots(1,2,figsize=figsize)
    y_pd_all = []
    y_nd_all = []

    dur_int = np.float(dur)*100 # convert from seconds to tens of milliseconds
    
    if plot: component_colors = cycle(component_colors)
        
        
    

    """ Loop through components/cells """
    for trace,coefs,name in zip(x_data.T,fit_dict['coefs'].T,list_keys):
        
        if plot: c = next(component_colors)
        # trace is a single trace, e.g. flash data for Tm1
        # coefs is coefficients for all locations

        """

        one problem with using fit_dict['x_data'].T, is that because the fit_dict is chopped at 1 second,
        we don't get the full effects of longer hyperpolarization
        """

        y_pd = np.zeros(x_data.shape[0]+5000-1)
        y_nd = np.zeros(x_data.shape[0]+5000-1)
        if plot: fig1,ax1 = plt.subplots(1,2,figsize=figsize) 
        """ Loop through spatial locations """
        for i,coef in enumerate(coefs):

            delta = np.zeros(5000)
            delta[int(i*dur_int)] = 1
            y_pd += np.convolve(coef*trace,delta)
            #plt.plot(np.convolve(model[:,i],delta))



            delta = np.zeros(5000)
            delta[int((len(coefs)-i-1)*dur_int)] = 1
            y_nd += np.convolve(coef*trace,delta)

            if plot:
                ax1[0].plot(y_pd,color=c)
                ax1[1].plot(y_nd,color=c)


        y_pd_all.append(y_pd)
        y_nd_all.append(y_nd)

        if plot:
            ax1[0].plot(y_pd,color=c)
            ax1[1].plot(y_nd,color=c)
            ax1[0].set_xlim(0,500)
            ax1[1].set_xlim(0,500)
            ax1[0].set_ylim(ylims)
            ax1[1].set_ylim(ylims)
            ax1[0].set_title(name+' PD')
            ax1[1].set_title(name+' ND')

    y_pd_all_trace = np.sum(y_pd_all,axis=0)
    y_nd_all_trace = np.sum(y_nd_all,axis=0)

    if plot:
        for a in ax:
            a.set_xlim(0,500)
            a.set_ylim(ylims)


        """ plot final result """
        ax[0].plot(y_pd_all_trace,color=colors[0])
        ax[0].set_title('PD')
        ax[1].plot(y_nd_all_trace,color=colors[1])
        ax[1].set_title('ND')
        
        
        
        
        
        
    """ compare with real data """
    # downsample by 2 # << MAKE SURE THIS IS CORRECT
    y_pd_data = moving_data[width][dur]['pd_mean'][::2]
    y_nd_data = moving_data[width][dur]['nd_mean'][::2]
    
    # sample window, depending on length of trace

    if verbose: print('r2 window:',r2_window)
    
    
    """ version 1: scale by max PD """
    scale = np.max(y_pd_data)/np.max(y_pd_all_trace)
    print('simple scale', scale)
    
    """ version 2: use simple linear regression to scale 
    
    Need to concatenate PD and ND data, and concatenate PD and ND prediction
    """
    
    model = Lasso(alpha=0.0001,
                  fit_intercept=False,
                  precompute=True,
                  max_iter=1000,
                  positive=True, 
                  random_state=9999, 
                  selection='random')
    
    x_data = np.concatenate((y_pd_all_trace[:r2_window],y_nd_all_trace[:r2_window]),axis=0).reshape(-1,1)
    y_data = np.concatenate((y_pd_data[:r2_window],y_nd_data[:r2_window]),axis=0)
    
    print(x_data.shape)
    print(y_data.shape)
    model.fit(x_data, y_data)
    scale = model.coef_[0] # should only be one coefficient
    
    print('LR scale:',scale)
    
    pd_score=r2_score(y_pd_data[:r2_window],scale*y_pd_all_trace[:r2_window])
    nd_score=r2_score(y_nd_data[:r2_window],scale*y_nd_all_trace[:r2_window])

    if plot:
        ax[0].plot(y_pd_data[:r2_window])
        ax[0].plot(scale*y_pd_all_trace[:r2_window])

        ax[1].plot(y_nd_data[:r2_window])
        ax[1].plot(scale*y_nd_all_trace[:r2_window])

        
    moving_bar_dict = {}
    moving_bar_dict['pd'] = y_pd_all_trace
    moving_bar_dict['nd'] = y_nd_all_trace
    moving_bar_dict['scale'] = scale
    moving_bar_dict['pd_score'] = pd_score
    moving_bar_dict['nd_score'] = nd_score
    
    moving_bar_dict['pd_data_max'] = np.max(y_pd_data)
    moving_bar_dict['pd_pred_max'] = np.max(scale*y_pd_all_trace)
    moving_bar_dict['nd_data_max'] = np.max(y_nd_data)
    moving_bar_dict['nd_pred_max'] = np.max(scale*y_nd_all_trace)
    
    
        
    return moving_bar_dict