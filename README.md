## Flexible filtering by neural inputs supports motion computation across stimuli and states

This repository contains code related to the publication [Flexible filtering by neural inputs supports motion computation across stimuli and states](https://www.sciencedirect.com/science/article/pii/S0960982221013178) by Kohn, Portes et al. (Current Biology 2021)

The folders contain:
1. Preprocessed data for Tm1, Tm2, Tm4 and Tm9 (pickle files)
2. Code for analysis and plotting of figures 2,3 and 5

Please contact us with questions! j.portes@columbia.edu, rb3161@columbia.edu

N.B. A bioRxiv preprint of this article from April 2021 can be found under the title [State and stimulus dependence reconcile motion computation and the Drosophila connectome](https://www.biorxiv.org/content/10.1101/2021.04.17.440267v1)



## Data License

The data shared with this code is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. This license requires that you contact us before you use the data in your own research. In particular, this means that you have to ask for permission if you intend to publish a new analysis performed with this data (no derivative works-clause).
