""" Various Functions for taking average and plotting Flash Data

created: October 1, 2020

updated: January 21, 2021



The following functions were built specifically for short impulse flashes.
Jessie began collecting data for 20, 40, 80 and 160 ms flashes in July 2020.
She also collected some data at low contrast.

For better or for worse, these functions are very specific to the stimulus
construction and likely do not generalize well. We are not too worried about this,
as we are mostly done with our data collection and don't plan on incorporating novel stimuli.

These functions were incorporated into axolotl.traces on January 21, 2021

I was considering putting them in traces/analysis.py or traces/plotting.py but
decided to keep all the flash functions separate.

Functions
---------

Analysis:

* extract_flash_data() - extracts and averages flash responses

* flash_wn_pred() - predicts flash response from white noise filters


Plotting:

* plot_flash_data_saline_OA() - plots average flash responses of saline and OA simultaneously

* plot_flash_and_pred() - plots flash responses with white noise predicted responses

* plot_flash_contrast_data() - plots flash responses at both low and high contrast







"""


import warnings
warnings.filterwarnings("ignore")

import os
import datajoint as dj
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys
import pprint
import math
import pandas as pd
import time #counting for filter extraction
from itertools import cycle
from tqdm import tqdm

from scipy.optimize import curve_fit
from scipy.signal import lfilter, decimate
from scipy.stats import binned_statistic
from scipy import signal

from sklearn.metrics import r2_score
from sklearn.model_selection import ParameterGrid

# for pdf generation
from matplotlib.backends.backend_pdf import PdfPages

# Contains custom schema for ephys data
from axolotl.schema.recordings_simple_ephys import RecordingInfo,RecordingData,FFFAlignmentParams,FFFData, WNFilterParams, WNData, DGData

# Contains custom plotting functions for data lists queried from datajoint schema
from axolotl.plotting.ephysplot import plot_fff, plot_xt, plot_spatial, plot_temporal, plot_dg, plot_dg_peaks, format_fig_poster
from axolotl.plotting.ephysplot import plot_spatial_mean, plot_nonlin_mean, plot_dg_peaks_mean
from axolotl.traces.convolve import sigmoid, softplus # static nonlinear functions used here
from axolotl.traces.analysis import simple_gaussian, return_mean_temporal,return_mean_fff, return_mean_spatial, return_mean_nonlin, fit_nonlin, return_mean_dg_peaks
from axolotl.utils import events_binary,argclosest


def extract_flash_data(data,
                       stim=["FFF_uniform_20msimpulse_10int","FFF_uniform_40msimpulse_10int","FFF_uniform_80msimpulse_10int","FFF_uniform_160msimpulse_10int"],
                       detrend=False,
                       window=10,
                       baseline='start',
                       baseline_window = 5,
                       plot=False,
                       verbose=False,
                       skip_sweep_dict = {},
                       figsize=(4,4),
                       **kwargs):

    """ This function takes in a datajoint query object, extracts flashes and averages them

    Args
    ----
    data: datajoint query object
    stim: list of strings for datajoint query e.g. stim = ["FFF_uniform_20msimpulse_10int"]
    detrend: Boolean, if True, apply scipy.signal.detrend(,type='linear') to entire trace
    window: int, number of seconds to cut from each sweep before averaging. Max is 10 for this stimulus paradigm
    baseline: str, 'start' or 'end', if start, average all sweeps from initial value.
            If 'end', average sweeps after subtracting mean of last n (e.g. 5) seconds for each sweep
    baseline_window: int, last n (e.g. 5) seconds to be averaged for each sweep to then be subtracted
    plot: Boolean, if True plots individual traces used for average. This is useful for sanity check
    verbose: Boolean
    skip_sweep_dict: dict, keys are recording_id and subrecording number, values are "sweep" to skip.
        e.g. skip_sweep_dict={'200928-18':0,'200928-20':3}

    Returns
    -------
    flash_dict: Dictionary with keys 'mean' 'std' 'n' and 'sampling_rate' for each stimulus condition specified by input string list 'stim'

    A NOTE ABOUT DETREND: I am doing a linear detrend on the entire signal, however I don't think this has a strong
    effect, and can actually cause distortion when traces have large (and deviant) activity when the projector turns on
    before the stimulus starts

    """

    flash_dict = {}



    for s in stim:

        test = data & 'stimulus_name="'+s+'"'


        fff_data = (test).fetch(as_dict=True)

        if plot:
            fig,ax = plt.subplots(1,1,figsize=figsize)

        mn_all = []
        n = 1

        if len(fff_data) == 0: # empty list
            print('skip stim ' + s + ' (empyt list)')
            continue

        ff = fff_data[0]
        old_name = ff['recording_id']
        if verbose: print(old_name)


        for ff in fff_data:

            """ linear detrend of entire/full trace """
            if detrend:
                ephys_trace = signal.detrend(ff['ephys_trace'],type='linear')
            else:
                ephys_trace = ff['ephys_trace']

            new_name = ff['recording_id']
            if verbose: print('>>'+new_name+'-'+ff['subrecording_number']+ ' '+ff['bath_solution'])

            """ keep track of n cells """
            if old_name != new_name:
                n+=1
                old_name = new_name

            if verbose: print('n=',n)

            """ process photodiode for pulling out traces """
            photodiode = ff['photodiode_trace']
            sos = signal.butter(10, 15, 'lp', fs=1000, output='sos')
            filtered_photodiode = signal.sosfilt(sos, photodiode)
            filtered_photodiode /=np.max(filtered_photodiode)

            start_times = events_binary(filtered_photodiode,ff['timestamps'],direction='rising',sigma=10)
            pd_start_times = argclosest(ff['timestamps'],start_times)

            """ plot and save traces"""
            traces = []

            if verbose: print('>> '+ff['recording_id']+'-'+ff['subrecording_number'])


            for ii,t in enumerate(pd_start_times):

                """ unusual case of skipping one or more sweeps manually"""
                skip_key = ff['recording_id']+'-'+ff['subrecording_number']
                if (skip_key in skip_sweep_dict.keys()) and (skip_sweep_dict[skip_key] == ii):
                    if verbose: print('>> skip sweep {} for '.format(ii)+ff['recording_id']+'-'+ff['subrecording_number'])
                    continue

                tmp = ephys_trace[t:t+int(window/0.0002)] # sampling rate hardcoded

                #plt.plot(trace - trace[0])

                """ baseline set at beginning or by average of end """
                if baseline == 'start':
                    traces.append(tmp-tmp[0])
                if baseline == 'end':
                    traces.append(tmp-np.mean(tmp[-int(baseline_window/0.0002):])) # baseline average of last 5 seconds
            mn = np.mean(traces,axis=0)
            mn_all.append(mn)

            time = np.arange(0,len(mn))*0.0002

            if plot:
                plt.plot(time,mn,linewidth=2,label=ff['cell_type']+'-'+ff['recording_id']+'-'+ff['subrecording_number']+ ' '+ff['bath_solution'])
                std = np.std(traces,axis=0)
                plt.fill_between(time,mn+std,mn-std,alpha=0.2)

        flash_dict[ff['stimulus_name']+' '+ff['bath_solution']] = {}
        flash_dict[ff['stimulus_name']+' '+ff['bath_solution']]['mean'] = np.mean(mn_all,axis=0)
        # should combine std of both here >>>>>>>
        #Tm4_flash_dict[ff['stimulus_name']+' '+ff['bath_solution']]['mean'] = np.mean(mn_all,axis=0)
        flash_dict[ff['stimulus_name']+' '+ff['bath_solution']]['std'] = np.std(mn_all,axis=0)
        flash_dict[ff['stimulus_name']+' '+ff['bath_solution']]['n'] = n

        flash_dict[ff['stimulus_name']+' '+ff['bath_solution']]['sampling_rate'] = ff['sampling_rate']

        flash_dict[ff['stimulus_name']+' '+ff['bath_solution']]['all'] = mn_all

        if plot:
            ax = plt.gca()
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            plt.plot(time,np.mean(mn_all,axis=0),'k',linewidth=2)
            plt.title(s[12:])
            plt.xlabel('time (s)')
            plt.legend(bbox_to_anchor=(1.1,1.1))
            plt.ylim(-0.02,0.03)
            plt.xlim(-0.02,3)

    return flash_dict





""" Make white noise prediction of falsh data """


def flash_wn_pred(wn_dict,flash_dict=None,plot=False,c = 'saddlebrown',**kwargs):

    """

    Make flash predictions with white noise filters

    This uses the complete linear-nonlinear filter for each cell individually and predicts responses to 20,40,80,160 ms
    by convolution in time, summation in space, and finally a static nonlinearity


    Args
    ----
    wn_dict: Dictionary, extracted from datajoint query. Example keys are 'recording_id','complete_filter'
    flash_dict:
    plot: Boolean, if True plots wn prediction with and flash data
    c: color string (e.g. 'midnightblue' for Tm1)


    Returns
    -------
    wn_pred_dict: Dictionary, contains keys for 4 conditions: 20,40,80,160. Value for each key is a list of 1D temporal predictions
    """

    if (plot and flash_dict == None):
        raise Exception('For plotting, must specify flash_dict')

    wn_pred_dict = {}
    wn_pred_dict['20'] = []
    wn_pred_dict['40'] = []
    wn_pred_dict['80'] = []
    wn_pred_dict['160'] = []


    # loop throug WN filters
    for wn in wn_dict:

        if plot: fig,ax = plt.subplots(1,4,figsize=(12,3))

        """Plot WN filter prediction (without nonlinearity)"""

        r_id = wn['recording_id']
        print(r_id)

        #filt = wn['temporal_filter'].squeeze()
        filt = np.real(wn['complete_filter'])

        #pred10s_off = np.convolve(filt,-1*np.ones(10*100))
        t_pred = np.arange(0,3,0.01)
        pred20 = np.zeros((len(t_pred),filt.shape[1]))
        pred40 = np.zeros((len(t_pred),filt.shape[1]))
        pred80 = np.zeros((len(t_pred),filt.shape[1]))
        pred160 = np.zeros((len(t_pred),filt.shape[1]))

        for i in range(filt.shape[1]):
            pred20[:,i] = np.convolve(filt[:,i],-1*np.ones(2))[:len(t_pred)]
            pred40[:,i] = np.convolve(filt[:,i],-1*np.ones(4))[:len(t_pred)]
            pred80[:,i] = np.convolve(filt[:,i],-1*np.ones(8))[:len(t_pred)]
            pred160[:,i] = np.convolve(filt[:,i],-1*np.ones(16))[:len(t_pred)]

        if plot:
            ax[0].plot(t_pred,np.sum(pred20,axis=1),'k-',linewidth=2,label='20 wn filter linear pred')
            ax[1].plot(t_pred,np.sum(pred40,axis=1),'k-',linewidth=2,label='40 wn filter linear pred')
            ax[2].plot(t_pred,np.sum(pred80,axis=1),'k-',linewidth=2,label='80 wn filter linear pred')
            ax[3].plot(t_pred,np.sum(pred160,axis=1),'k-',linewidth=2,label='160 wn filter linear pred')




        # apply nonlin
        fit = fit_nonlin([wn],nonlin='softplus',sigma_weight = None,verbose=False)

        a = fit['popt'][0]
        b = fit['popt'][1]
        cc = fit['popt'][2]
        d = fit['popt'][3]
        k = fit['popt'][4]
        #plt.plot(x,sigmoid(x,k,L,x0,b),'o',linewidth=4)
        #plt.plot(x,sigmoid(x,k,L,x0,b),'-',color=c,linewidth=3,alpha=1)

        if plot:
            ax[0].plot(t_pred,softplus(np.sum(pred20,axis=1),a,b,cc,d,k),'r--',linewidth=2,label='wn filter nonlinear pred')
            ax[1].plot(t_pred,softplus(np.sum(pred40,axis=1),a,b,cc,d,k),'r--',linewidth=2)
            ax[2].plot(t_pred,softplus(np.sum(pred80,axis=1),a,b,cc,d,k),'r--',linewidth=2)
            ax[3].plot(t_pred,softplus(np.sum(pred160,axis=1),a,b,cc,d,k),'r--',linewidth=2)


        wn_pred_dict['20'].append(softplus(np.sum(pred20,axis=1),a,b,cc,d,k))
        wn_pred_dict['40'].append(softplus(np.sum(pred40,axis=1),a,b,cc,d,k))
        wn_pred_dict['80'].append(softplus(np.sum(pred80,axis=1),a,b,cc,d,k))


        # SPECIFIC TO TM9 - BE CAREFUL HERE

        """ something is wrong with 190812 nonlinearity, keep it linear..."""
        if r_id == '190812':
            wn_pred_dict['160'].append(np.sum(pred160,axis=1))
        else:
            wn_pred_dict['160'].append(softplus(np.sum(pred160,axis=1),a,b,cc,d,k))




        """ Plot flash Data """
        for k,tr in flash_dict.items():
            if 'saline' in k:

                time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002

                #c = next(colors)
                if plot:
                    if '20' in k:
                        ax[0].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                        ax[0].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                        ax[0].set_title('20 ms')
                    if '40' in k:
                        ax[1].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                        ax[1].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                        ax[1].set_title('40 ms')
                    if '80' in k:
                        ax[2].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                        ax[2].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                        ax[2].set_title('80 ms')
                    if '160' in k:
                        ax[3].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                        ax[3].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                        ax[3].set_title('160 ms')




        if plot:
            for a in ax:
                a.set_ylim(-0.03,0.03)
                a.spines['top'].set_visible(False)
                a.spines['right'].set_visible(False)
                a.legend(frameon=False)
                a.get_legend().remove()

            plt.tight_layout()
            plt.show()


    return wn_pred_dict





""" PLOTTING FUNCTIONS """



""" Plot 20,40,80 and 160 flashes with and without OA """


def plot_flash_data_saline_OA(flash_dict,colors= ['darkmagenta','violet'],**kwargs):

    """

    This function plots saline and OA flashes for 20, 40, 80 and 160 ms stored as mean and std in dict

    Args
    ----
    flash_dict: Dictionary, extracted via the function 'extract_flash_data'.
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'

    Returns
    -------
    fig: Figure handle

    """

    plt.rcParams.update({'font.size': 14})

    if 'fig' in kwargs.keys():
        fig = kwargs['fig']
        ax = fig.get_axes()
    else:
        fig,ax = plt.subplots(1,4,figsize=(12,3))

    count = 0
    for k,tr in flash_dict.items():

        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002


        if count < 4:
            c = colors[0]
            count+=1
        else:
            c = colors[1]


        if '20' in k:
            ax[0].plot(time,tr['mean'],color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[0].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
            ax[0].set_title('20 ms')
        if '40' in k:
            ax[1].plot(time,tr['mean'],color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[1].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
            ax[1].set_title('40 ms')
        if '80' in k:
            ax[2].plot(time,tr['mean'],color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[2].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
            ax[2].set_title('80 ms')
        if '160' in k:
            ax[3].plot(time,tr['mean'],color=c,linewidth=3,label='(n='+str(tr['n'])+')')
            ax[3].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
            ax[3].set_title('160 ms')

    for i,a in enumerate(ax):
        a.set_ylim(-0.012,0.02)
        a.set_xlim(-0.1,3)
        a.legend(frameon=False)

        if i >0:
            a.axis('off')

        else:
            a.spines['top'].set_visible(False)
            a.spines['right'].set_visible(False)
            a.set_ylabel('V')
            a.set_xlabel('time (s)')

    #print('>>TM4')


#     figure_folder = '/mnt/engram/notebooks/jportes/DS-Project-Paper/Figures/Figures-Gilliam-2020-9-4/'
#     """ Save figure """
#     var = input("Save figures? (y/n): ")
#     if var == 'y':
#         fig.savefig(figure_folder+'flash-OA-Tm4.pdf')
#         print('saved figure: ',figure_folder+'flash-OA-Tm4.pdf')

    return fig





def plot_flash_and_pred(wn_pred_dict,flash_dict,bath_solution='saline',colors = ['darkmagenta','violet'],**kwargs):

    """

    This function plots saline (or OA) flashes with white noise filter predictions for 20, 40, 80 and 160 ms stored as mean and std in dict

    Args
    ----
    wn_pred_dict: Dictionary, extracted from flash_wn_pred function.
                Dictionary contains keys for 4 conditions: 20,40,80,160. Value for each key is a list of 1D temporal predictions

    flash_dict: Dictionary, extracted via the function 'extract_flash_data'.
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'
    bath_solution: str, either 'saline' or 'OA'
    colors: list of two color strings (e.g. ['darkmagenta','violet'])

    Returns
    -------
    fig: Figure handle

    """

    if 'fig' in kwargs.keys():
        fig = kwargs['fig']
        ax = fig.get_axes()
    else:
        fig,ax = plt.subplots(1,4,figsize=(12,3))

    mean20 = np.mean(wn_pred_dict['20'],axis=0)
    mean40 = np.mean(wn_pred_dict['40'],axis=0)
    mean80 = np.mean(wn_pred_dict['80'],axis=0)
    mean160 = np.mean(wn_pred_dict['160'],axis=0)

    std20 = np.std(wn_pred_dict['20'],axis=0)
    std40 = np.std(wn_pred_dict['40'],axis=0)
    std80 = np.std(wn_pred_dict['80'],axis=0)
    std160 = np.std(wn_pred_dict['160'],axis=0)

    t_pred = np.arange(0,3,0.01)

    c1 = 'black'

    """ Plot flash Data """
    count=0
    for k,tr in flash_dict.items():

        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002


        if count < 4:
            c = colors[0]
            count+=1
        else:
            c = colors[1]

        if bath_solution in k:
            #c = next(colors)
            if '20' in k:
                ax[0].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                ax[0].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)

                ax[0].plot(t_pred,mean20,'--',label=k,color=c1,linewidth=3)
                ax[0].fill_between(t_pred,mean20+std20,mean20-std20,color=c1,alpha=0.2)
                ax[0].set_title('20 ms')
            if '40' in k:
                ax[1].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                ax[1].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)

                ax[1].plot(t_pred,mean40,'--',label=k,color=c1,linewidth=3)
                ax[1].fill_between(t_pred,mean40+std40,mean40-std40,color=c1,alpha=0.2)
                ax[1].set_title('40 ms')
            if '80' in k:
                ax[2].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                ax[2].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)

                ax[2].plot(t_pred,mean80,'--',label=k,color=c1,linewidth=3)
                ax[2].fill_between(t_pred,mean80+std80,mean80-std80,color=c1,alpha=0.2)
                ax[2].set_title('80 ms')
            if '160' in k:
                ax[3].plot(time,tr['mean'],label=k,color=c,linewidth=3)
                ax[3].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)

                ax[3].plot(t_pred,mean160,'--',label=k,color=c1,linewidth=3)
                ax[3].fill_between(t_pred,mean160+std160,mean160-std160,color=c1,alpha=0.2)
                ax[3].set_title('160 ms')



    for i,a in enumerate(ax):
        a.set_ylim(-0.012,0.025)
        a.set_xlim(-0.1,3)
        if i >0:
            a.axis('off')

        else:
            a.spines['top'].set_visible(False)
            a.spines['right'].set_visible(False)
            a.legend(['flash data','WN pred'],frameon=False)
            a.set_ylabel('V')
            a.set_xlabel('time (s)')




    # figure_folder = '/mnt/engram/notebooks/jportes/DS-Project-Paper/Figures/Figures-Gilliam-2020-9-4/'
    # """ Save figure """
    # var = input("Save figures? (y/n): ")
    # if var == 'y':
    #     fig.savefig(figure_folder+'flash-wnpred-Tm9.pdf')
    #     print('saved figure: ',figure_folder+'flash-wnpred-Tm9.pdf')

    return fig






def plot_flash_contrast_data(flash_dict_low,
                             flash_dict_high,
                             bath_solution='saline',
                             colors= ['dimgray','violet'],
                             **kwargs):

    """ This function plots flashes for low contrast and high contrast stored in separate dicts. Assumes only 20 ms and 160 ms data


    Args
    ----
    flash_dict_low: Dictionary, extracted via the function 'extract_flash_data'. Should contain low contrast data
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int_lowcontrast0to01 saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'

    flash_dict_high: Dictionary, extracted via the function 'extract_flash_data'. Should contain high contrast data
                The dictionary primary keys should be the complete identifies for stimulus (e.g. 'FFF_uniform_20msimpulse_10int saline')
                The second/nested keys in the dictionary should contain keys 'mean', 'std', 'n' and 'sampling_rate'

    bath_solution: str, 'saline' or OA
    colors: list of 2 color strings


    Returns
    -------
    fig: Figure handle


    """

    plt.rcParams.update({'font.size': 14})

    fig,ax = plt.subplots(1,2,figsize=(6,3))
    count = 0


    for k,tr in flash_dict_high.items():

        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002

        c = colors[1]


        if bath_solution in k:
            if '20' in k:
                ax[0].plot(time,tr['mean'],color=c,linewidth=3,label='0-1 (n='+str(tr['n'])+')')
                ax[0].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                ax[0].set_title('20 ms')

            if '160' in k:
                ax[1].plot(time,tr['mean'],color=c,linewidth=3,label='0-1 (n='+str(tr['n'])+')')
                ax[1].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                ax[1].set_title('160 ms')


    for k,tr in flash_dict_low.items():

        time = np.arange(0,len(tr['mean']))*tr['sampling_rate'] # should be 0.0002


        c = colors[0]

        if bath_solution in k:
            if '20' in k:
                ax[0].plot(time,tr['mean'],color=c,linewidth=3,label='0-0.1 (n='+str(tr['n'])+')')
                ax[0].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                ax[0].set_title('20 ms')

            if '160' in k:
                ax[1].plot(time,tr['mean'],color=c,linewidth=3,label='0-0.1 (n='+str(tr['n'])+')')
                ax[1].fill_between(time,tr['mean']+tr['std'],tr['mean']-tr['std'],color=c,alpha=0.2)
                ax[1].set_title('160 ms')

    for i,a in enumerate(ax):
        a.set_ylim(-0.012,0.02)
        a.set_xlim(-0.1,3)
        a.legend(frameon=False)

        if i >0:
            a.axis('off')

        else:
            a.spines['top'].set_visible(False)
            a.spines['right'].set_visible(False)
            a.set_ylabel('V')
            a.set_xlabel('time (s)')


    return fig
